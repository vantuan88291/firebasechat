package com.tuan88291.firebasechat

import java.util.Date

/**
 * Created by tuan on 27/12/2017.
 */

class Chatmessage {
    var messageText: String? = null
    var messageUser: String? = null
    var messageUserId: String? = null
    var messageTime: Long = 0

    constructor(messageText: String, messageUser: String, messageUserId: String) {
        this.messageText = messageText
        this.messageUser = messageUser
        messageTime = Date().time
        this.messageUserId = messageUserId
    }

    constructor() {

    }
}