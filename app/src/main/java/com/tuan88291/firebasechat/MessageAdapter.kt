package com.tuan88291.firebasechat

import android.text.format.DateFormat
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.firebase.ui.database.FirebaseListAdapter
import com.google.firebase.database.DatabaseReference

/**
 * Created by tuan on 27/12/2017.
 */

class MessageAdapter(private val activity: MainActivity, modelClass: Class<Chatmessage>, modelLayout: Int, ref: DatabaseReference) : FirebaseListAdapter<Chatmessage>(activity, modelClass, modelLayout, ref) {

    override fun populateView(v: View?, model: Chatmessage, position: Int) {
        val messageText = v!!.findViewById<View>(R.id.message_text) as TextView
        val messageUser = v.findViewById<View>(R.id.message_user) as TextView
        val messageTime = v.findViewById<View>(R.id.message_time) as TextView

        messageText.text = model.messageText
        messageUser.text = model.messageUser

        // Format the date before showing it
        messageTime.text = DateFormat.format("dd-MM-yyyy (HH:mm)", model.messageTime)
    }

    override fun getView(position: Int, view: View?, viewGroup: ViewGroup): View? {
        var view = view
        val chatMessage = getItem(position)
        if (chatMessage.messageUserId == activity.loggedInUserName)
            view = activity.layoutInflater.inflate(R.layout.item_out_message, viewGroup, false)
        else
            view = activity.layoutInflater.inflate(R.layout.item_in_message, viewGroup, false)

        //generating view
        populateView(view, chatMessage, position)

        return view
    }

    override fun getViewTypeCount(): Int {
        // return the total number of view types. this value should never change
        // at runtime
        return 2
    }

    override fun getItemViewType(position: Int): Int {
        // return a value between 0 and (getViewTypeCount - 1)
        return position % 2
    }
}
