package com.tuan88291.firebasechat

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.text.format.DateFormat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.database.FirebaseListAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot



class MainActivity : AppCompatActivity() {
    private var adapter: FirebaseListAdapter<Chatmessage>? = null
    private var listView: ListView? = null
    var loggedInUserName = ""
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        stopService(Intent(baseContext, Service_chat::class.java))
        //find views by Ids
        val fab = findViewById<View>(R.id.fab) as FloatingActionButton
        val input = findViewById<View>(R.id.input) as EditText
        listView = findViewById<View>(R.id.list) as ListView

        if (FirebaseAuth.getInstance().currentUser == null) {
            // Start sign in/sign up activity
            startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .build(), SIGN_IN_REQUEST_CODE)
        } else {
            // User is already signed in, show list of messages
            showAllOldMessages()
        }

        fab.setOnClickListener {
            if (input.text.toString().trim { it <= ' ' } == "") {
                Toast.makeText(this@MainActivity, "Please enter some texts!", Toast.LENGTH_SHORT).show()
            } else {
                FirebaseDatabase.getInstance()
                        .reference
                        .push()
                        .setValue(Chatmessage(input.text.toString(),
                                FirebaseAuth.getInstance().currentUser!!.displayName!!,
                                FirebaseAuth.getInstance().currentUser!!.uid)
                        )
                input.setText("")
            }
        }







    }


    override fun onResume() {
        super.onResume()
        stopService(Intent(baseContext, Service_chat::class.java))

    }

    override fun onStop() {
        super.onStop()
        startService(Intent(baseContext, Service_chat::class.java))
        Toast.makeText(baseContext, "stop", Toast.LENGTH_LONG).show()



    }
    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(baseContext, "destroy", Toast.LENGTH_LONG).show()


        startService(Intent(baseContext, Service_chat::class.java))
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_sign_out) {
            AuthUI.getInstance().signOut(this)
                    .addOnCompleteListener {
                        Toast.makeText(this@MainActivity, "You have logged out!", Toast.LENGTH_SHORT).show()
                        finish()
                    }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Signed in successful!", Toast.LENGTH_LONG).show()
                showAllOldMessages()
            } else {
                Toast.makeText(this, "Sign in failed, please try again later", Toast.LENGTH_LONG).show()

                // Close the app
                finish()
            }
        }
    }

    private fun showAllOldMessages() {
        loggedInUserName = FirebaseAuth.getInstance().currentUser!!.uid
        Log.d("Main", "user id: " + loggedInUserName)

        adapter = MessageAdapter(this@MainActivity, Chatmessage::class.java, R.layout.item_in_message,
                FirebaseDatabase.getInstance().reference)
        listView!!.adapter = adapter
    }

    companion object {
        private val SIGN_IN_REQUEST_CODE = 111
    }
}
