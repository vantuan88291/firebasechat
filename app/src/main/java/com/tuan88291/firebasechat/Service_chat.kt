package com.tuan88291.firebasechat

import android.app.Service
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Handler
import android.os.IBinder
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewListener
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager
import java.text.DateFormat
import java.util.*

/**
 * Created by tuan on 28/12/2017.
 */

class Service_chat : Service() , FloatingViewListener {
    override fun onFinishFloatingView() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTouchFinished(isFinishing: Boolean, x: Int, y: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var mFloatingViewManager: FloatingViewManager? = null

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (mFloatingViewManager != null) {
            return Service.START_STICKY
        }
        FirebaseDatabase.getInstance().reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {


                var lastsms: String? = null
                var name: String? = null
                var idu: String? = null
                for (postSnapshot in dataSnapshot.children) {
                    lastsms = postSnapshot.children.iterator().next().getValue().toString()
                     name =  postSnapshot.child("messageUser").getValue().toString()
                    idu = postSnapshot.child("messageUserId").getValue().toString()
                }

///


                destroy()
                val metrics = DisplayMetrics()
                val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
                windowManager.defaultDisplay.getMetrics(metrics)
                val inflater = LayoutInflater.from(baseContext)
                val iViewc = inflater.inflate(R.layout.widget_chathead, null, false)

                val iconView = iViewc.findViewById<View>(R.id.info_text) as ImageView
                iconView.setOnClickListener {


                    Toast.makeText(baseContext,"ok", Toast.LENGTH_LONG).show()
                    destroy()
                    //show(this)


                }

                mFloatingViewManager = FloatingViewManager(baseContext, this@Service_chat)
                mFloatingViewManager!!.setFixedTrashIconImage(R.drawable.ic_trash_fixed)
                mFloatingViewManager!!.setActionTrashIconImage(R.drawable.ic_trash_action)
                val options = FloatingViewManager.Options()
                options.overMargin = (16 * metrics.density).toInt()
                options.floatingViewX = metrics.widthPixels

                options.floatingViewY = metrics.heightPixels / 2

                mFloatingViewManager!!.addViewToWindow(iconView, options)




            ///




            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

        return Service.START_REDELIVER_INTENT
    }

    private fun destroy() {
        if (mFloatingViewManager != null) {
            mFloatingViewManager!!.removeAllViewToWindow()
            mFloatingViewManager = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        destroy()

    }
}